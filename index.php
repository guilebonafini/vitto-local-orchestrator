<?php
    require __DIR__.'/vendor/autoload.php';

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as Response;
    use GuzzleHttp\Client;

    $app = new \Slim\App(
        new \Slim\Container([
            'settings' => [
                'displayErrorDetails' => true,
            ],
        ])
    );

    $version_app = 'v1';

    $app->map(['GET', 'POST', 'PUT', 'DELETE'], '/{microservice}/{routes:.*}', function (Request $request, Response $response, array $args) use ($version_app) {
        
        $client = new Client();

        $postData = (array) json_decode($request->getBody());
        $postData['x-user-key'] = $request->getHeader('x-user-key')[0] ?? "";
        $clientName = $request->getHeader('x-api-env-name')[0] ?? "";
        $params = $request->getQueryParams();

        if(!empty($params)) {
            foreach($params as $k => $v) {
                $postData[$k] = $v;
            }
        }

        $guzzleRequest = new \GuzzleHttp\Psr7\Request('POST', 'http://vitto-'.$args['microservice'].'.local/'.$version_app.'/'.$args['routes']);
        $guzzleResponse = $client->send($guzzleRequest, [
            'json' => [
                'client' => $clientName,
                'request' => [
                    'data' => $postData
                ]
            ]
        ]);
        $guzzleResp = json_decode((string) $guzzleResponse->getBody())->response;
        $guzzleResp->success = $guzzleResp->code == 200;
        return $response->withJson($guzzleResp);
    });
    $app->run();